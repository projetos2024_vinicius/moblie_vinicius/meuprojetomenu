import React from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function MenuPrincipal() {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate("Botao1")}
        style={styles.Botoes}
      >
        <Text style={styles.Text}>Botão 1</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("Botao2")}
        style={styles.Botoes}
      >
        <Text style={styles.Text}>Botão 2</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("Botao3")}
        style={styles.Botoes}
      >
        <Text style={styles.Text}>Botão 3</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("Botao4")}
        style={styles.Botoes}
      >
        <Text style={styles.Text}>Botão 4</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("Botao5")}
        style={styles.Botoes}
      >
        <Text style={styles.Text}>Botão 5</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  Botoes: {
    margin: 10,
    width: 200,
    height: 80,
    backgroundColor: "blue",
    justifyContent:'center',
    alignItems:'center',
    borderRadius:30,

  },
  Text:{
    color:'white',
    fontFamily: 'Roboto', 
    fontSize: 26
  }
});
