import React from "react";
import { StyleSheet, Text, View, ScrollView, Button } from "react-native";

export default function Botao3({ navigation }) {
  // Função para gerar os elementos de texto
  const generateTextElements = () => {
    const textElements = [];
    for (let i = 1; i <= 50; i++) {
      textElements.push(
        <Text key={i} style={styles.text}>{`Elemento ${i}`}</Text>
      );
    }
    return textElements;
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        {generateTextElements()}
      </ScrollView>
      <Button title="Voltar" onPress={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  text: {
    fontSize: 16,
    marginBottom: 10,
  },
});
