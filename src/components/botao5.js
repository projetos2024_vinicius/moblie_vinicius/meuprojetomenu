import React from "react";
import { StyleSheet, View, Text, ScrollView, TouchableOpacity, Button } from "react-native";

export default function Botao5({ navigation }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.touchableOpacity}>
        <Text style={styles.text}>Não faço nada :D</Text>
      </TouchableOpacity>
      <Button title="Voltar" onPress={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  touchableOpacity: {
    width: 200,
    height: 200,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    marginBottom: 20,
  },
  text: {
    color: 'white',
    fontSize: 25,
  },
});
