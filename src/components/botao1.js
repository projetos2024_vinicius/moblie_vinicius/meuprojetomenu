import React from "react";
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from "react-native";
import { useState } from "react";

export default function Botao1({ navigation }) {
  const [text, setText] = useState("");
  const [numLetras, setNumLetras] = useState('0')
  function click(){
    setNumLetras(text.length)
  }
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Teste do contador de caracteres</Text>
      <TextInput
      placeholder="Digite Algo"
      style={styles.input}
      value={text}
      onChangeText={(textInput) => setText (textInput) }
      />
      <Button
      title="Click"
      onPress={() => {click()}}
      />
      {numLetras > 0 ? 
      <Text  style={styles.text}>{numLetras}</Text>
       : 
       <Text></Text>}
      <Button title="Voltar" onPress={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
},

  text:{
    fontSize:24,
    fontWeight:'bold',
  },
  input:{
    borderWidth:1,
    width:"80%",
    borderColor:'gray',
    padding:10,
    marginVertical:10,

  }
});
