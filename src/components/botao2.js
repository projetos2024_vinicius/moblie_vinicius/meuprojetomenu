import React, { useState } from "react";
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button, TouchableOpacity } from "react-native";

export default function Botao2({ navigation }) {
  const [contador, setContador] = useState(0); // Inicializando o estado do contador como 0

  // Função para adicionar um ao contador
  const adicionarUm = () => {
    setContador(contador + 1);
  };

  return (
    <View style={styles.container}>
      <Text style={{ color: 'black', fontSize: 20, margin: 30 }}>Contador: {contador}</Text>
      <TouchableOpacity
        style={{ backgroundColor: 'blue', width: 80, height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 50, marginBottom: 20 }} // Adicionei marginBottom aqui
        onPress={adicionarUm}>
        <Text style={{ color: 'white', fontSize: 20 }}>click</Text>
      </TouchableOpacity>
      <Button title="Voltar" onPress={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
