import React from "react";
import { StyleSheet, Text, View, ScrollView, Button } from "react-native";

export default function Botao4({ navigation }) {
  // Função para renderizar as mensagens de saudação
  const renderMensagens = () => {
    return (
      <View>
        <Text style={styles.text}>Olá mundo</Text>
        <Text style={styles.text}>Hello World</Text>
        <Text style={styles.text}>Hallo Welt</Text>
        <Text style={styles.text}>Bonjour le monde</Text>
      </View>

      
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        {renderMensagens()}
      </ScrollView>
      <Button title="Voltar" onPress={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  scrollViewContent: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 16,
    marginBottom: 10,
  },
});
