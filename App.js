import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MenuPrincipal from './src/layout/menuPrincipal'; // Importe o componente do menu
import Botao1 from './src/components/botao1';
import Botao2 from './src/components/botao2';
import Botao3 from './src/components/botao3';
import Botao4 from './src/components/botao4';
import Botao5 from './src/components/botao5';
// Importe outras telas conforme necessário

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={MenuPrincipal} options={{ headerShown: false }} />
        <Stack.Screen name="Botao1" component={Botao1} />
        <Stack.Screen name="Botao2" component={Botao2} />
        <Stack.Screen name="Botao3" component={Botao3} />
        <Stack.Screen name="Botao4" component={Botao4} />
        <Stack.Screen name="Botao5" component={Botao5} /> 
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
});
